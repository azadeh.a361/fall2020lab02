
//Azadeh Ahmadi(1811386)
package eclipse;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manufacturer,int numberGears,double maxSpeed){
		this.manufacturer=manufacturer;
		this.numberGears=numberGears;
		this.maxSpeed=maxSpeed;
	}
	
	public String getManufacturer(){
		String manufacturer=this.manufacturer;
		return manufacturer;}
	
	public int getNumberGears(){
		int numberGears=this.numberGears;
		return numberGears;}
	
	public double getMaxSpeed(){
		double  maxSpeed=this.maxSpeed;
		return  maxSpeed;}
	
	public String toString(){
		String s="Manufacture:";
		s=s+this.manufacturer;
		s=s+", Number of Gears:";
		s=s+this.numberGears;
		s=s+", MaxSpeed :";
		s=s+this.maxSpeed;
		return s;
	}
	
	
	
	

}
